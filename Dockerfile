
# BUILD: docker build -t josephg/boilerplate-lib .
# RUN: docker run -p 4433:4433 -v /db:/boilerplate-library/db josephg/boilerplate-lib 

FROM mkenney/npm:7.0-alpine

RUN yarn global add polymer-cli
RUN yarn global add firebase-tools

CMD ["/usr/local/bin/npm", "start"]
